
import Square from './components/Square'
import style from './App.module.css'

function App() {
    
    return (
        <main className={style.play}>
            
            <h1 className={style.play__title}>Tic Tac Toe</h1>
            <div className="board">
            <div className="board__row">
            <Square value='1' />
            <Square value='2'/>
            <Square value='3' />
            </div>
            <div className="board__row">
            <Square value='4' />
            <Square value='5' />
            <Square value='6'/>
            </div>
            <div className="board__row" >
               <Square value='7' />
            <Square value='8' />
            <Square value='9'/> 
            </div>
            </div>
       
        </main>
    )
}
export default App;