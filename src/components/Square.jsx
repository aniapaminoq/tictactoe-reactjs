import style from './Square.module.css'

function Square({value}) {
    return (
        <button className={style.square}>{ value}</button>
   )
}
export default Square;